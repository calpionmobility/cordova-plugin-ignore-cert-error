---
title: Ignore SSL Cert Error
description: Plugin to ignore the SSL Cert Error is HTTPS calls
---

# cordova-plugin-ignore-cert-error

Plugin to ignore the SSL Cert Error is HTTPS calls

## Supported Platforms

- iOS

## Preferences

#### config.xml

-  __IgnoreSSLCertError__ (boolean, default to `false`). Indicates wherether
   to ignore the SSL Cert Error or not.

	<preference name="IgnoreSSLCertError" value="true" />



/********* CDVIgnoreCertError.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <Cordova/CDVAppDelegate.h>

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
	CDVAppDelegate *appDelegate = nil;
	NSString *ignoreSSLCertError = nil;
	BOOL bRet = NO;

	appDelegate = (CDVAppDelegate *) [UIApplication sharedApplication].delegate;
	ignoreSSLCertError = [appDelegate.viewController.settings objectForKey:[@"IgnoreSSLCertError" lowercaseString]];

	bRet = [ignoreSSLCertError boolValue];

	return bRet;
}
@end
